# Dolby Atmos User Interface Only Magisk Module

## DISCLAIMER
- Dolby apps and blobs are owned by Dolby™.
- The MIT license specified here is for the Magisk Module, not for Dolby apps and blobs.

## Descriptions
- Dolby Atmos equalizer user interface only for any ROM that has in-built Dolby Audio Processing service.

## Screenshots
- https://t.me/androidryukimodsdiscussions/66074

## Requirements
- Android 9 and up
- Supported ROM in-built Dolby Audio Processing dms-hal-2-0 or dms-hal-1-0 service
- Magisk installed

## Installation Guide & Download Link
- Install this module https://www.pling.com/p/1742547/ via Magisk app only
- Reboot

## Optionals
- https://t.me/androidryukimodsdiscussions/60861
- https://t.me/androidryukimodsdiscussions/2616

## Troubleshootings
- https://t.me/androidryukimodsdiscussions/29836
- https://t.me/androidryukimodsdiscussions/2617

## Support & Bug Report
- https://t.me/androidryukimodsdiscussions/2618
- If you don't do above, issues will be closed immediately

## Tested on
- OnePlus 8 Pro stock ROM Android 11

## Credits and contributors
- https://t.me/viperatmos
- https://t.me/androidryukimodsdiscussions
- You can contribute ideas about this Magisk Module here: https://t.me/androidappsportdevelopment

## Thanks for Donations
- This Magisk Module is always will be free but you can however show us that you are care by making a donations:
- https://ko-fi.com/reiryuki
- https://www.paypal.me/reiryuki
- https://t.me/androidryukimodsdiscussions/2619


